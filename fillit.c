/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smonroe <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 01:14:21 by smonroe           #+#    #+#             */
/*   Updated: 2018/07/20 13:54:08 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		main(int ac, char **av)
{
	char	*str;
	char	***cube;
	char	**grid;

	if (ac != 2)
		print_error(0);
	str = read_file(av[1]);
	validate_file(str);
	cube = make_cube(str);
	if (cube == NULL)
		print_error(1);
	cube = check_cube(cube);
	grid = make_grid(26);
	if (grid == NULL)
		print_error(1);
	call_back(grid, cube);
	print_grid(grid);
	free_grid(grid);
	free_cube(cube);
	return (0);
}

void	call_back(char **grid, char ***cube)
{
	int vals[6];
	int total;
	int i;

	total = 0;
	while (cube[total])
		total++;
	i = 2;
	while (i * i < total * 4)
		i++;
	vals[0] = i;
	vals[1] = 0;
	vals[2] = total - 1;
	vals[3] = 0;
	vals[4] = 0;
	while (!(backtrack(grid, cube, vals)))
		vals[0]++;
}

int		backtrack(char **grid, char ***cube, int *vals)
{
	int x;
	int y;

	y = -1;
	while (++y < vals[0])
	{
		x = -1;
		while (++x < vals[0])
		{
			vals[3] = y;
			vals[4] = x;
			if (place_grid(grid, vals, cube[vals[1]]))
			{
				if (vals[1] < vals[2])
					vals[1]++;
				else
					return (1);
				if (backtrack(grid, cube, vals))
					return (1);
				undo_place(grid, --vals[1]);
			}
			undo_place(grid, vals[1]);
		}
	}
	return (0);
}

void	free_grid(char **grid)
{
	int	x;

	x = -1;
	while (grid[++x])
		free(grid[x]);
	free(grid);
}
