/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mang.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smonroe <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/28 00:08:43 by smonroe           #+#    #+#             */
/*   Updated: 2018/05/23 14:21:53 by smonroe          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#define VARS int	c; int	y; int	x;

char	**zero_piece(char **piece)
{
	VARS;
	c = 1;
	y = 0;
	while (c)
	{
		x = -1;
		while (++x < 4)
			if (piece[y][x] != '.')
				c = 0;
		if (c)
			piece = move_up(piece);
	}
	c = 1;
	while (c)
	{
		x = -1;
		while (++x < 4)
			if (piece[x][y] != '.')
				c = 0;
		if (c)
			piece = move_left(piece);
	}
	return (piece);
}

char	**move_up(char **piece)
{
	int	x;
	int	y;

	y = -1;
	while (++y < 3)
	{
		x = -1;
		while (++x < 4)
			piece[y][x] = piece[y + 1][x];
	}
	x = -1;
	while (++x < 4)
		piece[3][x] = '.';
	return (piece);
}

char	**move_left(char **piece)
{
	int	x;
	int	y;

	x = -1;
	while (++x < 3)
	{
		y = -1;
		while (++y < 4)
			piece[y][x] = piece[y][x + 1];
	}
	y = -1;
	while (++y < 4)
		piece[y][3] = '.';
	return (piece);
}

int		get_max(char **bloc)
{
	int	x;
	int	y;
	int	max;

	max = 0;
	y = -1;
	while (bloc[++y])
	{
		x = -1;
		while (bloc[y][++x])
			if (bloc[y][x] != '.')
			{
				if (y > max)
					max = y;
				if (x > max)
					max = x;
			}
	}
	return (max);
}
