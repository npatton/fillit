#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: smonroe <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/25 01:08:27 by smonroe           #+#    #+#              #
#    Updated: 2018/05/26 18:12:11 by npatton          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = fillit

SRC = fillit.c file.c cube.c grid.c mang.c

LIB = -L libft/ -lft

$(NAME):
	make -C libft
	gcc -o $(NAME) -Wall -Werror -Wextra $(SRC) $(LIB)

all: $(NAME)

clean:
	rm -rf *~ \#*\#
	make clean -C libft

fclean: clean
	rm -rf fillit
	make fclean -C libft

re: fclean all
