/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smonroe <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 03:59:41 by smonroe           #+#    #+#             */
/*   Updated: 2018/05/26 18:46:48 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	*read_file(char *file)
{
	int		i;
	int		fd;
	char	pipe[1];
	char	*stream;

	i = 0;
	stream = NULL;
	stream = (char *)malloc(sizeof(char) * 21 * 26);
	if (stream)
	{
		fd = open(file, O_RDONLY);
		if (fd == -1)
			print_error(1);
		while (read(fd, pipe, 1))
			if (pipe[0] == '\n' || pipe[0] == '#' || pipe[0] == '.')
				stream[i++] = pipe[0];
			else
				print_error(1);
		stream[i] = '\0';
		if (close(fd) == -1)
			print_error(1);
	}
	if (!stream[0])
		print_error(1);
	return (stream);
}

int		check_line(char *s, int i)
{
	if (!((s[i - 1] == '.' || s[i - 1] == '#')
		&& (s[i - 2] == '.' || s[i - 2] == '#')
		&& (s[i - 3] == '.' || s[i - 3] == '#')
		&& (s[i - 4] == '.' || s[i - 4] == '#')))
		print_error(1);
	if (s[i + 1] == '\n' && s[i + 2] == '\0')
		print_error(1);
	return (1);
}

void	validate_bloc(char *s, int i, int t)
{
	int	c;
	int	b;

	if (t > 1)
		if (s[i - 21] != '\n')
			print_error(1);
	if (!(s[i - 5] == '\n' && s[i - 10] == '\n' && s[i - 15] == '\n'))
		print_error(1);
	check_line(s, i);
	check_line(s, i - 5);
	check_line(s, i - 10);
	check_line(s, i - 15);
	c = -1;
	b = 0;
	while (++c < 20)
		if (s[i - c] == '#')
			b++;
	if (b != 4)
		print_error(1);
}

void	validate_file(char *s)
{
	int	i;
	int	n;
	int	t;

	i = 0;
	n = 0;
	t = 0;
	while (s[i] != '\0')
	{
		if (s[i] == '\n')
			if (++n % 4 == 0)
			{
				validate_bloc(s, i++, ++t);
				n = 0;
			}
		if (n > 4)
			print_error(1);
		i++;
	}
	if (n != 0 || t > 26)
		print_error(1);
}

void	print_error(int m)
{
	if (m == 0)
		write(1, "usage: ./fillit [file]\n", 23);
	if (m == 1)
		write(1, "error\n", 6);
	exit(0);
}
